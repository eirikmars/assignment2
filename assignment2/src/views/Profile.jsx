import ProfileClearHistory from "../components/Profile/ProfileClearHistory"
import ProfileLogout from "../components/Profile/ProfileLogout"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileHistory from "../components/Profile/ProfileHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth.jsx"

const Profile = () => {

    const { user } = useUser()

    return (
        <>
        
        <div className='split left'>
            <section className='profileLeft'>
                <h1>Profile</h1>
                <img className='userPhotoBig'  src={ 'img/profile.png' } alt=""/>
                <h1>{ user.username }</h1>
                <ProfileLogout/>
            </section>
                
        </div>

        <div className='split right'>
            <section className='translateBox'>
                <section className='topSectionTranslate'>
                    <ProfileHeader username={ user.username }/>
                    <h2>Your latest translations:</h2>
                </section>

                <section className='bottomSectionTranslate'>
                    <ProfileHistory translations={ user.translations }/>
                    <ProfileClearHistory/>
                </section>
            </section>
            </div>
        
        </>
    )
}

export default withAuth(Profile)