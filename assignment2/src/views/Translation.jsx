import TranslationForm from "../components/Translations/TranslationForm"
import withAuth from "../hoc/withAuth"

const Translation = () => {

    return (
        <>
        <div className='backgroundTranslate'>
        <div className='site'>
        <TranslationForm />
        </div>
        </div>
        </>
    )
}

export default withAuth(Translation)