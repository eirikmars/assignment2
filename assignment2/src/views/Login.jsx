import LoginForm from "../components/Login/LoginForm"

const Login = () => {
    return (
        <>
        <div className='background'>
        <div className='site'>
            <section className='completeTop'>
                <img className='bigPhoto' src={ 'img/lilRobo.png' } alt=""/>
                <section className='topSection'>
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </section>
            </section>

            <section className='whitebox'>
                <LoginForm/>
            </section>
            </div></div>
        </>
    )
}

export default Login