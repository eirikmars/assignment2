import { useUser } from "../../context/UserContext"
import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useEffect } from "react"
import { updateUserTranslations } from "../../api/user"

const ProfileClearHistory = () => {
    const { user, setUser } = useUser()
    
    useEffect(() => {
        storageSave(STORAGE_KEY_USER, user)
    },[user])
    

    /**
     * When the user clicks the clear history button, the user's translations are set to an empty
     * array, and the user's translations are updated in the database.
     */
    const handleClearHistory = async () => {
        user.translations = []
        setUser({ id: user.id, username: user.username, translations: [] })
        updateUserTranslations(user, [])
    }

    return (
        <button onClick={ handleClearHistory }>Clear history</button>
    )
}
export default ProfileClearHistory