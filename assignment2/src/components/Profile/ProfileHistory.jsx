import ProfileHistoryItem from "./ProfileHistoryItem"

const ProfileHistory = ({ translations }) => {
    
    /* Creating a list of ProfileHistoryItem components. */
    const translationList = translations.slice(0,10).map(
            (translation, index) => <ProfileHistoryItem key={ index + "-" + translation } translation={ translation } />
        )

    return (
        <section className='translation'>
            <ul>{ translationList }</ul>
        </section>
    )    
}
export default ProfileHistory