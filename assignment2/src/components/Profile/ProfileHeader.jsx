const ProfileHeader = ({ username }) => {
    return (
        <header>
            <p>Welcome back { username }</p>
        </header>
    )
}
export default ProfileHeader