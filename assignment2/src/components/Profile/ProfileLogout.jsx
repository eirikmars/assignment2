import { useUser } from "../../context/UserContext"
import { storageDelete } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"

const ProfileLogout = () => {
    const { setUser } = useUser()
    
    /**
     * If the user clicks "OK" on the confirm dialog, then delete the user from local storage and set
     * the user to null.
     */
    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <button onClick={ handleLogoutClick }>Logout</button>
    )
}
export default ProfileLogout