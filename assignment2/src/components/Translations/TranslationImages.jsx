const TranslationImages = ({ translation }) => {
    const images = []

    for (let i = 0; i < translation.length; i++) {
        if(/\S/.test(translation[i])) {
            images.push(<img alt={ `${translation[i]}`} key={ i } src={ `img/${translation[i]}.png` }/>)
        }
    }

    return(
        <>
            <div>{ images }</div>
        </>
    )
}

export default TranslationImages