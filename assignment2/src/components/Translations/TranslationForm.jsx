import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";
import TranslationImages from "./TranslationImages";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { updateUserTranslations } from "../../api/user";

const TranslationForm = () => {
    const { register, handleSubmit, formState: { errors }  } = useForm();
    const { user, setUser } = useUser()

    const [ trans, setTranslation ] = useState("");

    useEffect(() => {
        storageSave(STORAGE_KEY_USER, user)
        updateUserTranslations(user, user.translations)
    },[user])

    /**
     * If the translation is not empty, then set the translation to the translation, add the
     * translation to the user's translations, and set the user to the user with the new translation.
     */
    const onSubmit = async ({ translation }) => {
        if(translation.length > 0) {
          setTranslation(translation)
          user.translations.unshift(translation)
          setUser({id: user.id, username: user.username, translations: user.translations})
        }
    }
    
    /* A function that returns a span if the input is illegal. */
    const errorMessage = (() => {
        if (!errors.translation) {
            return null
        }
        if (errors.translation.type === "pattern") {
            return <span>Illegal input</span>
        }
    }) ()
    
    return (
    <><section className="translateForm">
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset className='addPadd'>
          <h2 htmlFor="translation">What do you want to translate?</h2>
          <div className='field'>
          <input
            className='widerField'
            type="text"
            name="translation"
            {...register("translation", {
                pattern: {
                    value: /^[A-Za-z\s]+$/g,
                    message: 'Invalid input',
                },
            })}
          />
          { errorMessage }
          <button type="submit">Translate</button>
          </div>
          </fieldset>
      </form>
    </section>
      <section className='translateBox'>
            <h1>Translation to sign language:</h1>
            <TranslationImages translation={trans.toLowerCase()}/>
        </section>    
    </>
  );
}

export default TranslationForm