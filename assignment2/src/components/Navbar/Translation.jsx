import TranslationForm from "../components/Translations/TranslationForm"
import withAuth from "../hoc/withAuth"

const Translation = () => {

    return (
        <>
        <h1>translations</h1>
        <TranslationForm />
        </>
    )
}

export default withAuth(Translation)