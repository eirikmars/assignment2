import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

const Navbar = () => {

const { user } = useUser()

    return (
        <>
        <header className='header'>
            <section className='headerIcon'>
                { user !== null && <img className='smallPhoto' src={ 'img/lilRobo.png' } alt=""/> }
                <h3>Lost in Translation</h3>

            </section>
            
            <section className='headerIcon'>
                <section className='profileIcon'>  
                    <nav>
                        { user !== null &&
                                    <NavLink to="/profile"> <img className='userPhoto'  src={ '/img/profile.png' } alt=""/></NavLink>
                        }
                    </nav>
                    { user !== null && <NavLink id = "navlink-text"> {user.username} </NavLink> }
                </section>

                <section className='translateIcon'>
                    <nav>
                        { user !== null &&
                                    <NavLink to="/translations"> <img className='userPhoto'  src={ 'img/sign.png' } alt=""/></NavLink>
                        }
                    </nav>
                    { user !== null && <NavLink id = "navlink-text" to="/translations" >Translate</NavLink> }
                </section>
            </section>
        </header>
        </>
    )
}

export default Navbar