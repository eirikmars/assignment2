import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user"
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const usernameConfig = {
  required: true,
  minLength: 3,
};

const LoginForm = () => {
  // Hooks
  const { register, handleSubmit, formState: { errors }  } = useForm();
  const { user, setUser } = useUser()
  const navigate = useNavigate()

  // Local State
  const [ loading, setLoading ] = useState(false)
  const [ apiError, setApiError ] = useState(null)

  // Side Effects
  useEffect(() => {
    if (user !== null) {
      navigate("translations")
    }
  }, [ user, navigate ])

  // Event Handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [ error, userResponse ] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }

    setLoading(false)

  };

  /* A function that returns a span element if the username is required or too short. */
  const errorMessage = (() => {
    if (!errors.username) {
        return null
    }

    if (errors.username.type === "required") {
        return <span>Username is required</span>
    }
    
    if (errors.username.type === "minLength") {
        return <span>Username is too short(min 3 characters)</span>
    }

  }) ()

  return (
    <>
    <div className='bottomSection'>
      <h3>What is your name?</h3>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <section className='field'>
          <label className='fieldContent' htmlFor="username">Username:</label>
          <input
            className='usernameField'
            type="text"
            placeholder="johnjoe"
            {...register("username", usernameConfig)}
          />
          <button type="submit" disabled ={ loading }>Continue</button>
          </section>
          { errorMessage }
          </fieldset>
        
        { loading && <p>Logging in...</p> }
        { apiError && <p>{ apiError }</p> }
      </form>
      </div>
    </>
  );
};

export default LoginForm;
