import { createHeaders } from "./index"
const apiUrl = process.env.REACT_APP_API_URL

/**
 * It takes a username as an argument, makes a request to the API, and returns an array with the first
 * element being an error message (if there is one) and the second element being the data returned from
 * the API (if there is any).
 * @param username - The username to check for.
 * @returns An array with two elements. The first element is an error message or null. The second
 * element is the data or null.
 */
export const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error("Could not complete request.")
        }
        const data = await response.json() 
        return [ null, data ]
    } catch (error) {
        return [ error.message, null ]
    }
}

/**
 * It takes a username as an argument, and returns an array with two elements: the first element is an
 * error message, and the second element is the data returned from the server.
 * 
 * If there is an error, the first element will be a string with the error message, and the second
 * element will be an empty array.
 * 
 * If there is no error, the first element will be null, and the second element will be the data
 * returned from the server.
 * @param username - string
 * @returns An array with two elements. The first element is an error message or null. The second
 * element is the data returned from the server.
 */
export const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error("Could not create user with username " + username)
        }
        const data = await response.json() 
        return [ null, data ]
    } catch (error) {
        return [ error.message, [] ]
    }
}

/**
 * If the user exists, return the user, otherwise create the user and return the user.
 * @param username - string
 * @returns An array with two elements. The first element is an error object or null. The second
 * element is a user object or null.
 */
export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)
    if (checkError !== null) {
        return [ checkError, null ]
    }

    if (user.length > 0) {
        return [ null, user.pop() ]
    }

    return await createUser(username)
}

/**
 * It takes a user object and an array of translations, and updates the user's translations in the
 * database.
 * @param user
 * @param translations
 */
export const updateUserTranslations = async (user, translations) => {
    try{
        const response = await fetch(`${apiUrl}/${user.id}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: translations
            })
        })
        if(!response.ok){
            throw new Error("Could not update translations")
        }
    } catch(error){
        console.error(error.message)
    }
}