import {BrowserRouter, Route, Routes } from "react-router-dom";
import './App.css';
import Login from './views/Login';
import Translation from "./views/Translation";
import Profile from "./views/Profile";
import Navbar from "./components/Navbar/Navbar";

function App() {
  
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={ <Login />} />
          <Route path="/profile" element={ <Profile/>} />
          <Route path="/translations" element={ <Translation/> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;