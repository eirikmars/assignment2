# Assignment2

An online sign language translator as a Single Page Application using the React framework. 

## Usage
The first thing a user sees is the “Login page”.

### Login page
To use the app, login with your username. If your username doesn't exist, it will be created and saved in the translations API.

### Translation page
After a succesful login, you will be transfered to the translation page where you can translate words into sign language. You type in the input box at the top of the page and you must click on the “translate” button or press enter to trigger the translation. 

### Profile page
To go to your profile, click the profile icon in the top right corner. On your profile you can see your last 10 translations, clear your translation history and logout of your account. To return to the translation page, click the translation button in the top right corner. 

## Maintained by:
- [Eirik Marstrander](https://www.gitlab.com/eirikmars) 
- [Emma Kaulum Sanding](https://www.gitlab.com/emmakaulum)
